import { Router} from "express";
import refereeController from "../controller/refereeController";

class RefereeRoutes {
    public router: Router =  Router();

    constructor() {
        this.config();
    }
    config(): void{
        this.router.get('/', refereeController.listReferee);
        this.router.get('/:id', refereeController.getOneReferee);
        this.router.post('/', refereeController.createReferee);
        this.router.put('/:id', refereeController.updateReferee);
        this.router.delete('/:id', refereeController.deleteReferee);
    }
}
const refereeRoutes = new RefereeRoutes()
export default refereeRoutes.router;
