import express, { Application } from 'express';
import morgan from 'morgan';
import cors from 'cors'

import indexRoutes from './routes/indexRoutes';
import jugadoresRoutes from './routes/jugadoresRoutes';
import referreRoutes from './routes/refereeRoutes';
import equipmentRoutes from "./routes/equipmentRoute";

class Server {
    public app: Application;

    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }

    config(): void{
        this.app.set('port', process.env.PORT || 3001);
        this.app.use(morgan('dev'));
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: false}));

    }

    routes(): void{
        this.app.use('/', indexRoutes);
        this.app.use('/apiv1/jugadores',jugadoresRoutes);
        this.app.use('/apiv1/referee',referreRoutes);
        this.app.use('/apiv1/equipment', equipmentRoutes);
    }

    start(): void{
        this.app.listen(this.app.get('port'), ()=>{
            console.log('Server en Port' , this.app.get('port'));
        })
    }
    
}

const server = new Server();
server.start();
