"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const refereeController_1 = __importDefault(require("../controller/refereeController"));
class RefereeRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', refereeController_1.default.listReferee);
        this.router.get('/:id', refereeController_1.default.getOneReferee);
        this.router.post('/', refereeController_1.default.createReferee);
        this.router.put('/:id', refereeController_1.default.updateReferee);
        this.router.delete('/:id', refereeController_1.default.deleteReferee);
    }
}
const refereeRoutes = new RefereeRoutes();
exports.default = refereeRoutes.router;
