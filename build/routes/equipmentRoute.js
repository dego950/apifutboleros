"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const equipmentController_1 = __importDefault(require("../controller/equipmentController"));
class EquipmentRoute {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', equipmentController_1.default.listEquipment);
        this.router.get('/:id', equipmentController_1.default.getOneEquipment);
        this.router.post('/', equipmentController_1.default.createEquipment);
        this.router.put('/:id', equipmentController_1.default.updateEquipment);
        this.router.delete('/:id', equipmentController_1.default.deleteEquipment);
    }
}
const equipmentRoute = new EquipmentRoute();
exports.default = equipmentRoute.router;
