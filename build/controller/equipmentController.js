"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class EquipmentController {
    listEquipment(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const equipment = yield database_1.default.query('SELECT * FROM Equipo');
            res.json(equipment);
        });
    }
    getOneEquipment(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const equipment = yield database_1.default.query('SELECT * FROM Equipo WHERE idEquipos = ?', [id]);
            console.log(equipment.length);
            if (equipment.length > 0) {
                return res.json(equipment[0]);
            }
            res.status(404).json({ text: "The equipment doesn't exits" });
        });
    }
    createEquipment(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield database_1.default.query('INSERT INTO Equipo set ?', [req.body]);
            res.json({ message: 'equipment Saved' });
        });
    }
    updateEquipment(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const oldEquipment = req.body;
            yield database_1.default.query('UPDATE Equipo set ? WHERE idEquipos = ?', [req.body, id]);
            res.json({ message: "The equipment was Updated" });
        });
    }
    deleteEquipment(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query('DELETE FROM Equipo WHERE idEquipos = ?', [id]);
            res.json({ message: "The equipment was deleted" });
        });
    }
}
exports.equipmentController = new EquipmentController();
exports.default = exports.equipmentController;
