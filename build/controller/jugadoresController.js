"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class JugadoresController {
    list(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const jugadores = yield database_1.default.query('SELECT * FROM Jugador');
            res.json(jugadores);
        });
    }
    getOne(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const jugador = yield database_1.default.query('SELECT * FROM Jugador WHERE idJugador = ?', [id]);
            console.log(jugador.length);
            if (jugador.length > 0) {
                return res.json(jugador[0]);
            }
            res.status(404).json({ text: "The Player doesn't exits" });
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield database_1.default.query('INSERT INTO Jugador set ?', [req.body]);
            res.json({ message: 'Player Saved' });
        });
    }
    update(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const oldPlayer = req.body;
            yield database_1.default.query('UPDATE Jugador set ? WHERE idJugador = ?', [req.body, id]);
            res.json({ message: "The Player was Updated" });
        });
    }
    delete(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query('DELETE FROM Jugador WHERE idJugador = ?', [id]);
            res.json({ message: "The Player was deleted" });
        });
    }
}
const jugadoresController = new JugadoresController();
exports.default = jugadoresController;
