import { Request, Response } from 'express';
import pool from '../database';

class JugadoresController {

    public async list(req: Request, res: Response): Promise<void> {
        const jugadores = await pool.query('SELECT * FROM Jugador');
        res.json(jugadores);
    }

    public async getOne(req: Request, res: Response): Promise<any> {
        const { id } = req.params;
        const jugador = await pool.query('SELECT * FROM Jugador WHERE idJugador = ?', [id]);
        console.log(jugador.length);
        if (jugador.length > 0) {
            return res.json(jugador[0]);
        }
        res.status(404).json({ text: "The Player doesn't exits" });
    }

    public async create(req: Request, res: Response): Promise<void> {
        const result = await pool.query('INSERT INTO Jugador set ?', [req.body]);
        res.json({ message: 'Player Saved' });
    }

    public async update(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        const oldPlayer = req.body;
        await pool.query('UPDATE Jugador set ? WHERE idJugador = ?', [req.body, id]);
        res.json({ message: "The Player was Updated" });
    }

    public async delete(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        await pool.query('DELETE FROM Jugador WHERE idJugador = ?', [id]);
        res.json({ message: "The Player was deleted" });
    }
}

const jugadoresController = new JugadoresController();
export default jugadoresController;
