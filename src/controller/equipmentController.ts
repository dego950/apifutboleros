import { Request, Response } from 'express';
import pool from "../database";

class EquipmentController {

    public async listEquipment(req: Request, res: Response): Promise<void> {
        const equipment = await pool.query('SELECT * FROM Equipo');
        res.json(equipment);
    }

    public async getOneEquipment(req: Request, res: Response): Promise<any> {
        const { id } = req.params;
        const equipment = await pool.query('SELECT * FROM Equipo WHERE idEquipos = ?', [id]);
        console.log(equipment.length);
        if (equipment.length > 0) {
            return res.json(equipment[0]);
        }
        res.status(404).json({ text: "The equipment doesn't exits" });
    }

    public async createEquipment(req: Request, res: Response): Promise<void> {
        const result = await pool.query('INSERT INTO Equipo set ?', [req.body]);
        res.json({ message: 'equipment Saved' });
    }

    public async updateEquipment(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        const oldEquipment = req.body;
        await pool.query('UPDATE Equipo set ? WHERE idEquipos = ?', [req.body, id]);
        res.json({ message: "The equipment was Updated" });
    }

    public async deleteEquipment(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        await pool.query('DELETE FROM Equipo WHERE idEquipos = ?', [id]);
        res.json({ message: "The equipment was deleted" });
    }

}

export const equipmentController = new EquipmentController();
export default equipmentController;
