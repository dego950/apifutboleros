import { Router} from "express";
import jugadoresController from "../controller/jugadoresController";

class JugadoresRoutes {
    public router: Router =  Router();

    constructor() {
        this.config();
    }


    config(): void{
        this.router.get('/', jugadoresController.list);
        this.router.get('/:id', jugadoresController.getOne);
        this.router.post('/', jugadoresController.create);
        this.router.put('/:id',jugadoresController.update);
        this.router.delete('/:id',jugadoresController.delete);
    }
}

const jugadoresRoutes = new JugadoresRoutes()
export default jugadoresRoutes.router;
