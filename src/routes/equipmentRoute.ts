import { Router} from "express";
import equipmentController from "../controller/equipmentController";

class EquipmentRoute {
    public router: Router =  Router();

    constructor() {
        this.config();
    }
    config(): void{
        this.router.get('/', equipmentController.listEquipment);
        this.router.get('/:id', equipmentController.getOneEquipment);
        this.router.post('/', equipmentController.createEquipment);
        this.router.put('/:id', equipmentController.updateEquipment);
        this.router.delete('/:id', equipmentController.deleteEquipment);
    }
}
const equipmentRoute = new EquipmentRoute();
export default equipmentRoute.router;
