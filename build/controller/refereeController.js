"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const database_1 = __importDefault(require("../database"));
class RefereeController {
    listReferee(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const referee = yield database_1.default.query('SELECT * FROM Arbitros');
            res.json(referee);
        });
    }
    getOneReferee(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const referee = yield database_1.default.query('SELECT * FROM Arbitros WHERE idArbitro = ?', [id]);
            console.log(referee.length);
            if (referee.length > 0) {
                return res.json(referee[0]);
            }
            res.status(404).json({ text: "The Referee doesn't exits" });
        });
    }
    createReferee(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield database_1.default.query('INSERT INTO Arbitros set ?', [req.body]);
            res.json({ message: 'Referee Saved' });
        });
    }
    updateReferee(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const oldReferee = req.body;
            yield database_1.default.query('UPDATE Arbitros set ? WHERE idArbitro = ?', [req.body, id]);
            res.json({ message: "The Referee was Updated" });
        });
    }
    deleteReferee(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            yield database_1.default.query('DELETE FROM Arbitros WHERE idArbitro = ?', [id]);
            res.json({ message: "The Referee was deleted" });
        });
    }
}
const refereeController = new RefereeController();
exports.default = refereeController;
