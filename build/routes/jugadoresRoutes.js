"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const jugadoresController_1 = __importDefault(require("../controller/jugadoresController"));
class JugadoresRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.get('/', jugadoresController_1.default.list);
        this.router.get('/:id', jugadoresController_1.default.getOne);
        this.router.post('/', jugadoresController_1.default.create);
        this.router.put('/:id', jugadoresController_1.default.update);
        this.router.delete('/:id', jugadoresController_1.default.delete);
    }
}
const jugadoresRoutes = new JugadoresRoutes();
exports.default = jugadoresRoutes.router;
