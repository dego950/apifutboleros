import { Request, Response } from 'express';
import pool from '../database';


class RefereeController {

    public async listReferee(req: Request, res: Response): Promise<void> {
        const referee = await pool.query('SELECT * FROM Arbitros');
        res.json(referee);
    }

    public async getOneReferee(req: Request, res: Response): Promise<any> {
        const { id } = req.params;
        const referee = await pool.query('SELECT * FROM Arbitros WHERE idArbitro = ?', [id]);
        console.log(referee.length);
        if (referee.length > 0) {
            return res.json(referee[0]);
        }
        res.status(404).json({ text: "The Referee doesn't exits" });
    }

    public async createReferee(req: Request, res: Response): Promise<void> {
        const result = await pool.query('INSERT INTO Arbitros set ?', [req.body]);
        res.json({ message: 'Referee Saved' });
    }

    public async updateReferee(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        const oldReferee = req.body;
        await pool.query('UPDATE Arbitros set ? WHERE idArbitro = ?', [req.body, id]);
        res.json({ message: "The Referee was Updated" });
    }

    public async deleteReferee(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        await pool.query('DELETE FROM Arbitros WHERE idArbitro = ?', [id]);
        res.json({ message: "The Referee was deleted" });
    }

}
const refereeController = new RefereeController();
export default refereeController;


